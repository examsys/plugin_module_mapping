<?php
// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

use testing\unittest\unittestdatabase;
use PHPUnit\DbUnit\DataSet\YamlDataSet;

/**
 * Test mapping functions
 * 
 * @author Dr Joseph Baxter <joseph.baxter@nottingham.ac.uk>
 * @version 1.0
 * @copyright Copyright (c) 2016 onwards The University of Nottingham
 * @package tests
 */
class mappingtest extends unittestdatabase {
    /**
     * Set-up mapping table.
     */
    public function setUp() {
        $this->setup_db();
        $installfile = dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . "plugin_module_mapping" . DIRECTORY_SEPARATOR . 'db' . DIRECTORY_SEPARATOR . 'install.sql';
        \DBUtils::run_sql($installfile, $this->config->get('cfg_phpunit_db_user'), $this->config->get('cfg_phpunit_db_password'));
        $this->parentSetUp();
    }
    /**
     * Get init data set from yml
     * @return dataset
     */
    public function getDataSet() {
        return new YamlDataSet(dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . "plugin_module_mapping" . DIRECTORY_SEPARATOR . "unittest" . DIRECTORY_SEPARATOR  . "fixtures" . DIRECTORY_SEPARATOR . "mapping.yml");
    }
    /**
     * Get expected data set from yml
     * @param string $name fixture file name
     * @return dataset
     */
    public function get_expected_data_set($name) {
        return new YamlDataSet(dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . "plugin_module_mapping" . DIRECTORY_SEPARATOR . "unittest" . DIRECTORY_SEPARATOR  . "fixtures" . DIRECTORY_SEPARATOR . $name . ".yml");
    }
    /**
     * Test get mapping function - get campus code
     * @group mapping
     */
    public function test_get_mapping_campus() {
        $mapping = new plugins\mapping\plugin_module_mapping\plugin_module_mapping();
        // UK code.
        $this->assertEquals("COMP1007", $mapping->get_mapping("G51MCS"));
        // Malaysia code.
        $this->assertEquals("COMP1019", $mapping->get_mapping("G51MCS_UNMC"));
        // China code no mapping.
        $this->assertEquals("G51MCS_UNNC", $mapping->get_mapping("G51MCS_UNNC"));
        // Unknown code.
        $this->assertEquals("A12BCD_UNNC", $mapping->get_mapping("A12BCD_UNNC"));
        // Un-recognised code.
        $this->assertEquals("TEST", $mapping->get_mapping("TEST"));
    }
    /**
     * Test get mapping function - get saturn code
     * @group mapping
     */
    public function test_get_mapping_saturn() {
        $mapping = new plugins\mapping\plugin_module_mapping\plugin_module_mapping();
        // UK code.
        $this->assertEquals("G51MCS", $mapping->get_mapping("COMP1007"));
        // Malaysia code.
        $this->assertEquals("G51MCS", $mapping->get_mapping("COMP1019_UNMC"));
        // China code no mapping.
        $this->assertEquals("COMP1036_UNNC", $mapping->get_mapping("COMP1036_UNNC"));
        // Unknown code.
        $this->assertEquals("COMP1008_UNNC", $mapping->get_mapping("COMP1008_UNNC"));
        // Un-recognised code.
        $this->assertEquals("TEST", $mapping->get_mapping("TEST"));
    }
    /**
     * Test install mapping plugin - already installed on setup
     * @group mapping
     */
    public function test_install() {
        $mapping = new plugins\mapping\plugin_module_mapping\plugin_module_mapping();
        $this->assertEquals('OK', $mapping->install($this->config->get('cfg_phpunit_db_user'), $this->config->get('cfg_phpunit_db_password')));
        // Check tables are correct.
        $queryTable = $this->getConnection()->createQueryTable('plugins', 'SELECT component, version, type FROM plugins');
        $expectedTable = $this->get_expected_data_set('pluginconfig')->getTable("plugins");
        $this->assertTablesEqual($expectedTable, $queryTable);
        $queryTable = $this->getConnection()->createQueryTable('config', 'SELECT component, setting, value, type FROM config');
        $expectedTable = $this->get_expected_data_set('pluginconfig')->getTable("config");
        $this->assertTablesEqual($expectedTable, $queryTable);
        $mapping->uninstall($this->config->get('cfg_phpunit_db_user'), $this->config->get('cfg_phpunit_db_password'));
    }
    /**
     * Test uninstall mapping plugin - already installed on setup
     * @group mapping
     */
    public function test_uninstall() {
        $mapping = new plugins\mapping\plugin_module_mapping\plugin_module_mapping();
        $mapping->install($this->config->get('cfg_phpunit_db_user'), $this->config->get('cfg_phpunit_db_password'));
        $this->assertEquals('OK', $mapping->uninstall($this->config->get('cfg_phpunit_db_user'), $this->config->get('cfg_phpunit_db_password')));
        // Check tables are correct.
        $queryTable = $this->getConnection()->getRowCount('plugins');
        $this->assertEquals(0, $queryTable);
        $queryTable = $this->getConnection()->createQueryTable('config', 'SELECT component, setting, value, type FROM config');
        $expectedTable = $this->get_expected_data_set('nopluginconfig')->getTable("config");
        $this->assertTablesEqual($expectedTable, $queryTable);
    }
    /**
     * Test check plugin version
     * @group mapping
     */
    public function test_get_plugin_version() {
        $mapping = new plugins\mapping\plugin_module_mapping\plugin_module_mapping();
        $mapping->install($this->config->get('cfg_phpunit_db_user'), $this->config->get('cfg_phpunit_db_password'));
        $this->assertEquals($mapping->get_installed_version(), $mapping->get_plugin_version('plugin_module_mapping'));
        $mapping->uninstall($this->config->get('cfg_phpunit_db_user'), $this->config->get('cfg_phpunit_db_password'));
    }
}
