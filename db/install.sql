-- Create the table
CREATE TABLE IF NOT EXISTS `module_map` (
    `saturn` VARCHAR(11) NULL DEFAULT NULL,
    `campus` VARCHAR(13) NULL DEFAULT NULL,
    `country` VARCHAR(4) NOT NULL,
    UNIQUE INDEX `module_map_idx1` (`saturn`, `country`),
    UNIQUE INDEX `module_map_idx2` (`campus`, `country`)
) COLLATE='utf8_general_ci' ENGINE=InnoDB;