<?php
// This file is part of ExamSys
//
// ExamSys is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ExamSys is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ExamSys.  If not, see <http://www.gnu.org/licenses/>.

namespace plugins\mapping\plugin_module_mapping;

/**
* Mapping plugin helper file
* 
* @author Dr Joseph Baxter <joseph.baxter@nottingham.ac.uk>
* @copyright Copyright (c) 2016 onwards The University of Nottingham
*/

/**
 * Module mapping plugin.
 */
class plugin_module_mapping extends \plugins\plugins_mapping {
    /**
     * Name of the plugin;
     * @var string
     */
    protected $plugin = 'plugin_module_mapping';
    /**
     * Getting saturn<->campus module mapping
     * @param string $source source module code
     * @return string orginial $source if mapping not found, $target if mapping found
     */
    public function get_mapping($source) {
        // Check if source is campus solutions.
        preg_match("/^(?P<module>[A-Z]{4}([0-9]{4}|[A-Z0-9]{6}))(_(?P<country>UNNC|UNMC))?$/", $source, $info);
        if (count($info) > 0) {
            $s = 'campus';
            $t = 'saturn';
        } else {
            // Check is source is saturn.
            preg_match("/^(?P<module>[A-Z0-9]{6})(_(?P<country>UNNC|UNMC))?$/", $source, $info);
            if (count($info) > 0) {
                $s = 'saturn';
                $t = 'campus';
            } else {
                // Return source module id if naming convention not recognised.
                return $source;
            }
        }
        if (empty($info['country'])) {
            $info['country'] = "UNUK";
        }
        $sql = $this->db->prepare("SELECT $t FROM module_map WHERE $s = ? and country = ?");
        $sql->bind_param('ss', $info['module'], $info['country']);
        $sql->execute();
        $sql->store_result();
        $sql->bind_result($target);
        $sql->fetch();
        if ($sql->num_rows == 0) {
            $sql->close();
            // Return source module id if not target found.
            return $source;
        }
        $sql->close();
        if (empty($target)) {
            return $source;
        }
        return $target;
    }
}